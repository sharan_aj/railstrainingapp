class PostsUsersReadStatus < ActiveRecord::Migration[6.1]
  def change
    create_join_table :posts, :users
  end
end
