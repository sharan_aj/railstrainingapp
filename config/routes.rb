Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }
  root 'topics#index'
  resources :topics do
    resources :posts do
      resources :ratings, only: [:create]
      resources :comments, only: [:create, :destroy] do
        resources :user_comment_ratings, only: [:index, :create]
      end
    end
  end

  resources :tags
  get '/posts', to: 'posts#index'
  post '/topics/:topic_id/posts/:id/user_status', to: 'posts#user_status'
end
