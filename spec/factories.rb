FactoryBot.define do

  factory :topic do
    title { 'topic 1' }
  end

  factory :post do
    title { 'Post title' }
    body { 'Post body' }
  end

  factory :comment do
    body { 'this is comment body' }
  end

  factory :tag do
    name { 'science' }
  end

  factory :rating do
    point { 1 }
  end

  factory :user do
    email { 'sharan@example.com' }
    password { 'usersharan' }
  end

  factory :user_comment_rating do
    point { 1 }
  end
end
