require 'rails_helper'

RSpec.describe Rating, type: :model do
  before(:each) do
    @topic = create(:topic)
    @user = create(:user)
    @post = create(:post, topic_id: @topic.id, user_id: @user.id)
  end

  it 'ensures rating is false' do
    @rating = build(:rating, point: 1, post_id: @post.id)
    expect(@rating.save).to be(true)
  end

  it 'ensures rating is not true' do
    @rating = build(:rating, point: nil, post_id: @post.id)
    expect(@rating.save).to be(false)
    expect(@rating.errors.full_messages.first).to eq("Point can't be blank")
  end

  it 'ensures rating is only integer' do
    @rating = build(:rating, point: 1.2, post_id: @post.id)
    expect(@rating.save).to be(false)
    expect(@rating.errors.full_messages.first).to eq('Point must be an integer')
  end

  it 'ensures rating must be greater then or equal to 1' do
    @rating = build(:rating, point: 0, post_id: @post.id)
    expect(@rating.save).to be(false)
    expect(@rating.errors.full_messages.first).to eq('Point must be greater than or equal to 1')
  end

  it 'ensures rating must be less than or equal to 5' do
    @rating = build(:rating, point: 10, post_id: @post.id)
    expect(@rating.save).to be(false)
    expect(@rating.errors.full_messages.first).to eq('Point must be less than or equal to 5')
  end

  it 'ensures rating is not saved without post_id' do
    @rating = build(:rating, point: 1, post_id: nil)
    expect(@rating.save).to be(false)
  end

  it 'ensures rating is not saved if post id is not valid datatype' do
    @rating = build(:rating, point: 1, post_id: '')
    expect(@rating.save).to be(false)
  end
end
