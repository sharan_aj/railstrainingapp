require 'rails_helper'

RSpec.describe User, type: :model do

  it 'ensures created user is true' do
    user = create(:user)
    expect(user.email).to eq('sharan@example.com')
  end

  it 'ensures user creation is failed when create with existing email' do
    create(:user)
    user1 = build(:user)
    expect(user1.save).to be(false)
  end

  it 'ensures user creation is failed when create with blank email' do
    user1 = build(:user, email: '')
    user1.save
    expect(user1.errors.full_messages).to include("Email can't be blank")
  end

  it 'ensures user creation is failed when create with non valid email' do
    user1 = build(:user, email: 'sharan123')
    user1.save
    expect(user1.errors.full_messages).to include("Email is invalid")
  end

  it 'ensures created user password length is short' do
    user1 = build(:user, password: '123')
    user1.save
    expect(user1.errors.full_messages).to include('Password is too short (minimum is 6 characters)')
  end

  it 'ensures created user password is failed when create with nil' do
    user1 = build(:user, password: nil)
    user1.save
    expect(user1.errors.full_messages).to include("Password can't be blank")
  end

  it 'ensures created user password is failed when create with empty string' do
    user1 = build(:user, password: '')
    user1.save
    expect(user1.errors.full_messages).to include("Password can't be blank")
  end
end
