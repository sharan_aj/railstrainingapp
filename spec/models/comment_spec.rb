require 'rails_helper'

RSpec.describe Comment, type: :model do

  before(:each) do
    @topic = create(:topic)
    @user = create(:user)
    @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    @comment = create(:comment, post_id: @post.id, user_id: @user.id)
  end

  it 'ensures presence of body' do
    expect(@comment.body).to eq('this is comment body')
  end

  it 'record should return false' do
    comment = build(:comment, body: '', post_id: @post.id)
    expect(comment.save).to be(false)
    expect(comment.errors.full_messages).to include("Body can't be blank")
  end

  it 'ensures record should be valid' do
    expect(@comment).to be_valid
  end

  it 'ensures comment can be accessed by post' do
    expect(@post.comments.first).to eq(@comment)
  end
end
