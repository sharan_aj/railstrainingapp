require 'rails_helper'

RSpec.describe Topic, type: :model do

  it 'ensures topic is not saved and not valid' do
    topic = build(:topic, title: nil)
    expect(topic.save).to eq(false)
    expect(topic.errors.full_messages.first).to eq("Title can't be blank")
  end

  it 'ensures topic should be minimum of 3 characters' do
    topic = build(:topic, title: 'hi')
    expect(topic.save).to eq(false)
    expect(topic.errors.full_messages.first).to eq('Title is too short (minimum is 3 characters)')
  end

  it 'ensures topic is valid' do
    topic = create(:topic)
    expect(topic).to be_valid
    expect(topic.save).to eq(true)
  end
end
