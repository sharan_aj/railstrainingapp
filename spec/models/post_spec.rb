require 'rails_helper'

RSpec.describe Post, type: :model do

  before(:each) do
    @topic = create(:topic)
    @user = create(:user)
  end

  context 'posts validation test' do

    it 'ensures title and body' do
      post = build(:post, title: '', body: '', topic_id: @topic.id)
      expect(post.save).not_to be(true)
      expect(post.errors.full_messages).to include("Title can't be blank")
      expect(post.errors.full_messages).to include("Body can't be blank")
    end

    it 'ensures title must be present' do
      post = build(:post, title: '', body: 'body of the post', topic_id: @topic.id)
      expect(post.save).not_to be(true)
      expect(post.errors.full_messages).to include("Title can't be blank")
    end

    it 'ensures body must also be present' do
      post = build(:post, title: 'this is a post title', body: '', topic_id: @topic.id)
      expect(post.save).not_to be(true)
      expect(post.errors.full_messages).to include("Body can't be blank")
    end

  end

  it 'ensures title and body' do
    @post = build(:post, title: nil, body: nil, topic_id: @topic.id, user_id: @user.id)
    expect(@post.save).to be(false)
    expect(@post.errors.full_messages).to include("Title can't be blank")
    expect(@post.errors.full_messages).to include("Body can't be blank")
  end

  it 'ensures title and body has more than 3 characters' do
    @post = build(:post, title: 'hi', body: 'hi', topic_id: @topic.id, user_id: @user.id)
    expect(@post.save).to be(false)
    expect(@post.errors.full_messages).to include('Title is too short (minimum is 3 characters)')
    expect(@post.errors.full_messages).to include('Body is too short (minimum is 3 characters)')
  end

  it 'ensures post is valid' do
    @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    expect(@post).to be_valid
    expect(@topic.posts.count).to be(1)
    expect(@topic.posts.first.title).to eq('Post title')
  end

  it 'ensures image is not attached' do
    @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    @post.image.attach(nil)
    expect(@post.image.attached?).to be(false)
  end

  it 'ensures image is not supported type' do
    @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    @post.image.attach(io: File.open(Rails.root.join('spec', 'assets', 'sample_text.txt')),
                       filename: 'sample_text.txt',
                       content_type: 'text/plain')
    @post.save
    expect(@post.errors.full_messages.first).to eq('Image must be png or jpeg')
  end

  it 'ensures image is not supported type and not less than 1 mb' do
    @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    @post.image.attach(io: File.open(Rails.root.join('spec', 'assets', 'sample_pdf.pdf')),
                       filename: 'sample_pdf.pdf',
                       content_type: 'application/pdf')
    @post.save
    expect(@post.errors.full_messages).to include('Image size is too big,should be less than 1 MB')
    expect(@post.errors.full_messages).to include('Image must be png or jpeg')
    expect(@post.errors.full_messages.count).to eq(2)
  end

  it 'ensures image is and not less than 1 mb' do
    @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    @post.image.attach(io: File.open(Rails.root.join('spec', 'assets', 'sample_image.jpg')),
                       filename: 'sample_image.jpg',
                       content_type: 'image/jpeg')
    @post.save
    expect(@post.errors.full_messages).to include('Image size is too big,should be less than 1 MB')
  end

  it 'ensures image uploads successfully' do
    @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    @post.image.attach(io: File.open(Rails.root.join('spec', 'assets', 'post_image_1.jpeg')),
                       filename: 'post_image_1.jpeg',
                       content_type: 'image/jpeg')
    @post.save
    expect(@post.image.attached?).to eq(true)
  end

  context 'posts between specified range of date' do

    before(:each) do
      post1 = create(:post, topic_id: @topic.id, user_id: @user.id)
      post2 = create(:post, topic_id: @topic.id, user_id: @user.id)
      post3 = create(:post, topic_id: @topic.id, user_id: @user.id)
    end

    it 'returns 3 post for specfied range of date' do
      expect(Post.filter_date(Date.yesterday, Date.today.end_of_day).count).to eq(3)
    end

    it 'returns no post for specfied range of date' do
      expect(Post.filter_date('2022-04-21'.to_date, '2022-04-25'.to_date).count).to eq(0)
    end
  end

  context 'Counter Cache' do

    before(:each) do
      @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    end

    it 'ensures comments count is zero initially' do
      expect(@post.comments_count).to eq(0)
    end

    it 'ensures comments count is updated' do
      comment1 = create(:comment, post_id: @post.id, user_id: @user.id)
      comment2 = create(:comment, post_id: @post.id, user_id: @user.id)
      comment3 = create(:comment, post_id: @post.id, user_id: @user.id)
      @post.reload
      expect(@post.comments_count).to eq(3)
    end
  end

  context 'Ratings Average Caching' do

    before(:each) do
      @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    end

    it 'ensures comments count is zero initially' do
      expect(@post.rating_average).to eq(0.0)
    end

    it 'ensures ratings average is updated' do
      rating1 = create(:rating, point: 1, post_id: @post.id)
      rating2 = create(:rating, point: 5, post_id: @post.id)
      rating3 = create(:rating, point: 3, post_id: @post.id)
      @post.reload
      expect(@post.rating_average).to eq(@post.ratings.average(:point).to_f.floor(1))
    end
  end
end
