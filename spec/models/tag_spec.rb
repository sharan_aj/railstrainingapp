require 'rails_helper'

RSpec.describe Tag, type: :model do
  before(:each) do
    @topic = create(:topic)
    @user = create(:user)
    @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    @tag = create(:tag)
  end

  it 'ensures tag is valid' do
    expect(@tag).to be_valid
    expect(@tag.name).to eq('science')
  end

  it 'ensures tag is not true' do
    tag = build(:tag, name: '')
    expect(tag.save).not_to be(true)
    expect(tag.errors.full_messages).to include("Name can't be blank")
  end

  it 'ensures tag is unique' do
    tag1 = build(:tag)
    expect(tag1.save).not_to be(true)
    expect(tag1.errors.full_messages).to include('Name has already been taken')
  end

  it 'ensures tag can be assigned and accessed by post' do
    @post.tags << @tag
    expect(@post.tags.first).to eq(@tag)
  end
end
