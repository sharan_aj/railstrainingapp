require 'rails_helper'

RSpec.describe UserCommentRating, type: :model do

  before(:each) do
    @topic = create(:topic)
    @user = create(:user)
    @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    @comment = create(:comment, post_id: @post.id, user_id: @user.id)
  end

  it 'ensures rating is not true' do
    rating = build(:user_comment_rating, point: nil, comment_id: @comment.id, user_id: @user.id)
    expect(rating.save).to be(false)
    expect(rating.errors.full_messages.first).to eq("Point can't be blank")
  end

  it 'ensures rating is only integer' do
    rating = build(:user_comment_rating, point: 1.2, comment_id: @comment.id, user_id: @user.id)
    expect(rating.save).to be(false)
    expect(rating.errors.full_messages.first).to eq('Point must be an integer')
  end

  it 'ensures rating must be greater then or equal to 1' do
    rating = build(:user_comment_rating, point: 0, comment_id: @comment.id, user_id: @user.id)
    expect(rating.save).to be(false)
    expect(rating.errors.full_messages.first).to eq('Point must be greater than or equal to 1')
  end

  it 'ensures rating must be less than or equal to 5' do
    rating = build(:user_comment_rating, point: 10, comment_id: @comment.id, user_id: @user.id)
    expect(rating.save).to be(false)
    expect(rating.errors.full_messages.first).to eq('Point must be less than or equal to 5')
  end
end
