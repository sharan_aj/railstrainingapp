require 'rails_helper'

RSpec.describe 'UserCommentRatings', type: :request do

  before(:each) do
    @topic = create(:topic)
    @user = create(:user)
    @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    @comment = create(:comment, post_id: @post.id, user_id: @user.id)
    @rating1 = create(:user_comment_rating, comment_id: @comment.id, user_id: @user.id)
    @rating2 = create(:user_comment_rating, point: 3, comment_id: @comment.id, user_id: @user.id)
    sign_in @user
  end

  it 'response with status of ok in index page' do
    get topic_post_comment_user_comment_ratings_url(@topic, @post, @comment)
    expect(response).to have_http_status(:ok)
  end

  it 'renders index page' do
    get topic_post_comment_user_comment_ratings_url(@topic, @post, @comment)
    expect(response).to render_template(:index)
  end

  it 'renders index page with all ratings of that comment' do
    get topic_post_comment_user_comment_ratings_url(@topic, @post, @comment)
    expect(assigns(:ratings)).to eq(@comment.user_comment_ratings)
  end

  it 'renders index page with all ratings with the count of 2' do
    get topic_post_comment_user_comment_ratings_url(@topic, @post, @comment)
    expect(assigns(:ratings).count).to be(2)
  end

  it 'creates a record successfully and redirects to show post page' do
    post topic_post_comment_user_comment_ratings_url(@topic, @post, @comment), params: {
      user_comment_rating: {
        point: 2
      }
    }
    expect(response).to have_http_status(:found)
    expect(response).to redirect_to(topic_post_url(@topic, @post))
  end

  it 'fails to create a record and redirects to show post page with error' do
    post topic_post_comment_user_comment_ratings_url(@topic, @post, @comment), params: {
      user_comment_rating: {
        point: 0
      }
    }
    expect(response).to redirect_to(topic_post_url(@post.topic, @post))
    expect(@post.ratings.count).to be(0)
    expect(assigns(:rating).errors.full_messages.first).to eq('Point must be greater than or equal to 1')
    expect(flash[:info]).to eq('Point must be greater than or equal to 1')
  end
end