require 'rails_helper'

RSpec.describe 'Tags', type: :request do

  context 'unauthorized' do

    it 'returns 302 and redirects to sign in page' do
      get tags_url
      expect(response).to have_http_status(302)
      expect(response).to redirect_to(new_user_session_url)
    end
  end

  context 'tag routes authorized' do

    before(:each) do
      sign_in create(:user)
    end

    it 'ensures /tags route display all tags' do
      @tag1 = create(:tag, name: 'science1')
      @tag2 = create(:tag, name: 'art')
      get '/tags'
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include('Tags')
      expect(assigns(:tags)).to eq(Tag.all)
    end

  it 'ensures tags POST route creates a tag' do
    params = {
      tag: {
        name: 'new tag'
      }
    }
    post '/tags', params: params
    expect(response).to have_http_status(:found)
    expect(response).to redirect_to(tags_url)
    expect(Tag.first.name).to eq('new tag')
  end

  it 'ensures tags POST route not creates a tag with no name' do
    params = {
      tag: {
        name: ''
      }
    }
    post '/tags', params: params
    expect(response).to have_http_status(:found)
    expect(response).to redirect_to(tags_url)
    expect(flash[:error]).to eq("Name can't be blank")
    expect(Tag.all.count).to eq(0)
  end

    it 'ensures tag updated and redirected to tags route' do
      @tag = create(:tag)
      params = {
        tag: {
          name: 'art'
        }
      }
      put "/tags/#{@tag.id}", params: params
      expect(response).to redirect_to(tags_url)
      expect(flash[:notice]).to eq('Tag Updated Successfully')
      @tag.reload
      expect(@tag.name).to eq('art')
    end

    it 'ensures tag not updated and shows error' do
      @tag = create(:tag)
      params = {
        tag: {
          name: ''
        }
      }
      put "/tags/#{@tag.id}", params: params
      expect(response).to redirect_to(tags_url(id: @tag.id))
      expect(flash[:error]).to eq("Name can't be blank")
      @tag.reload
      expect(@tag.name).to eq('science')
    end

    it 'expect to be tag to be destroyed and redirect to tags route' do
      @tag = create(:tag)
      delete "/tags/#{@tag.id}"
      expect(response).to redirect_to(tags_url)
      expect(Tag.all).to be_empty
    end
  end
end
