require 'rails_helper'

RSpec.describe 'Comments', type: :request do

  before(:each) do
    @user = create(:user)
    sign_in @user
  end

  it 'is new post form rendered as js' do
    get '/users/edit', xhr: true
    expect(response).to render_template(:edit)
    expect(response.content_type).to eq('text/javascript; charset=utf-8')
  end

  it 'is new post form rendered as js' do
    put '/users', xhr: true, params: {
      user: {
        email: "sharan@example.com",
        password: "usersharan1",
        password_confirmation: "usersharan1",
        current_password: "usersharan"
      }
    }
    expect(response).to render_template(:update)
    expect(response.content_type).to eq('text/javascript; charset=utf-8')
  end
end