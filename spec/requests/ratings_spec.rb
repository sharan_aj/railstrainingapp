require 'rails_helper'

RSpec.describe 'Ratings', type: :request do
  before(:each) do
    @topic = create(:topic)
    @user = create(:user)
    @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    sign_in @user
  end

  it 'creates a record successfully and redirects to show post page' do
    post "/topics/#{@topic.id}/posts/#{@post.id}/ratings", params: {
      rating: {
        point: 2
      }
    }
    expect(response).to have_http_status(:found)
  end

  it 'redirects to topic post url' do
    post "/posts/#{@post.id}/ratings", params: {
      rating: {
        point: 2
      }
    }
    expect(response).to redirect_to(topic_post_url(@post.topic, @post))
  end

  it 'ratings table has to be count of 1' do
    post "/posts/#{@post.id}/ratings", params: {
      rating: {
        point: 2
      }
    }
    expect(@post.ratings.count).to be(1)
  end

  it 'rating record has a point of 2' do
    post "/posts/#{@post.id}/ratings", params: {
      rating: {
        point: 2
      }
    }
    expect(@post.ratings.first.point).to be(2)
  end

  it 'fails to create a record and redirects to show post page with error' do
    post "/topics/#{@topic.id}/posts/#{@post.id}/ratings", params: {
      rating: {
        point: 0
      }
    }
    expect(response).to redirect_to(topic_post_url(@post.topic, @post))
  end

  it 'rating table has to be count of zero' do
    post "/posts/#{@post.id}/ratings", params: {
      rating: {
        point: 0
      }
    }
    expect(@post.ratings.count).to be(0)
  end

  it "rating doesn't save and returns with error" do
    post "/posts/#{@post.id}/ratings", params: {
      rating: {
        point: 0
      }
    }
    expect(assigns(:rating).errors.full_messages.first).to eq('Point must be greater than or equal to 1')
  end
end
