require 'rails_helper'

RSpec.describe 'Posts', type: :request do

  require "cancan/matchers"

  context 'unauthorized user' do

    before(:each) do
      @topic = create(:topic)
      @user = create(:user)
      @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    end

    it 'redirects to sign in page' do
      get topic_posts_url(@topic)
      expect(response).to have_http_status(302)
      expect(response).to redirect_to(new_user_session_url)
    end

    it 'redirects to sign in page' do
      get new_topic_post_url(@topic)
      expect(response).to have_http_status(302)
      expect(response).to redirect_to(new_user_session_url)
    end

    it 'redirects to sign in page' do
      get topic_post_url(@topic, @post)
      expect(response).to have_http_status(302)
      expect(response).to redirect_to(new_user_session_url)
    end

    it 'redirects to sign in page' do
      get edit_topic_post_url(@topic, @post)
      expect(response).to have_http_status(302)
      expect(response).to redirect_to(new_user_session_url)
    end
  end

  context 'all posts and new route' do

    before(:each) do
      @topic = create(:topic)
      sign_in create(:user)
    end

    it 'displays post index page' do
      get "/topics/#{@topic.id}/posts"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
    end

    it 'displays post index page with error message' do
      get '/topics/200/posts'
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include("Topic doesn't exist")
    end

    it 'displays post new page' do
      get "/topics/#{@topic.id}/posts/new"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:new)
    end

    it 'displays post new page with error message' do
      get '/topics/200/posts/new'
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:new)
      expect(response.body).to include("Topic doesn't exist")
    end

    it 'create a new post and redirect to show page' do
      params = {
        post: {
          title: 'topic 1',
          body: 'this is a new post',
          tag_ids: [],
          tags_attributes: {
            name: ''
          }
        }
      }
      post "/topics/#{@topic.id}/posts", params: params
      expect(Post.all.count).to be(1)
      expect(response).to redirect_to(topic_post_url(@topic, Post.first))
      expect(response).to have_http_status(:found)
    end

    it "doesn't creates a new post and render form with errors" do
      params = {
        post: {
          title: 'h',
          body: 'hi',
          tag_ids: [],
          tags_attributes: {
            name: ''
          }
        }
      }
      post "/topics/#{@topic.id}/posts", params: params
      expect(Post.all.count).to be(0)
      expect(response).to render_template(:new)
      expect(response).to have_http_status(:unprocessable_entity)
      expect(assigns(:post).errors.count).to be(2)
    end

    it "doesn't creates a new post and render form with errors" do
      params = {
        post: {
          title: nil,
          body: nil,
          tag_ids: [],
          tags_attributes: {
            name: ''
          }
        }
      }
      post "/topics/#{@topic.id}/posts", params: params
      expect(Post.all.count).to be(0)
      expect(response).to render_template(:new)
      expect(response).to have_http_status(:unprocessable_entity)
      expect(assigns(:post).errors.full_messages).to include("Title can't be blank")
      expect(assigns(:post).errors.full_messages).to include("Body can't be blank")
    end
  end
  context 'show, edit, update and delete posts' do

    before(:each) do
      @topic = create(:topic)
      @user = create(:user)
      @post = create(:post, topic_id: @topic.id, user_id: @user.id)
      sign_in @user
    end

    it 'displays individual post in show page' do
      get "/topics/#{@topic.id}/posts/#{@post.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end

    it 'displays edit page form' do
      get "/topics/#{@topic.id}/posts/#{@post.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:edit)
    end

    it 'edits a post and redirects to show page' do
      params = { post: {
        title: 'post new',
        body: 'this is a body part'
      } }
      put "/topics/#{@topic.id}/posts/#{@post.id}", params: params
      @post.reload
      expect(@post.title).to eq('post new')
      expect(@post.body).to eq('this is a body part')
      expect(response).to redirect_to(topic_post_url(@topic, @post))
    end

    it "doesn't edits a post and render edit page with errors" do
      params = { post: {
        title: 'h',
        body: 'ad'
      } }
      put "/topics/#{@topic.id}/posts/#{@post.id}", params: params
      expect(response).to have_http_status(:unprocessable_entity)
      expect(response).to render_template(:edit)
      expect(assigns(:post).errors.count).to be(2)
    end

    it "doesn't edits a post and render edit page with errors" do
      params = { post: {
        title: nil,
        body: ''
      } }
      put "/topics/#{@topic.id}/posts/#{@post.id}", params: params
      expect(response).to have_http_status(:unprocessable_entity)
      expect(response).to render_template(:edit)
      expect(assigns(:post).errors.full_messages).to include("Title can't be blank")
      expect(assigns(:post).errors.full_messages).to include("Body can't be blank")
    end

    it 'deleting a post based on its id' do
      delete "/topics/#{@topic.id}/posts/#{@post.id}"
      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(topic_posts_url(@topic.id))
      expect(Post.all.count).to be(0)
    end
  end

  context 'Post and Patch/put routes' do

    before(:each) do
      @topic = create(:topic)
      @user = create(:user)
      @post = create(:post, topic_id: @topic.id, user_id: @user.id)
      @tag1 = create(:tag, name: 'hello')
      @tag2 = create(:tag, name: 'tags')
      sign_in @user
    end

    it 'ensures update a post with tags based on id' do
      params = {
        id: 1,
        post: {
          title: 'post new',
          body: 'this is a body part',
          tag_ids: [@tag1.id]
        }
      }
      put "/topics/#{@topic.id}/posts/#{@post.id}", params: params
      @post.reload
      expect(@post.title).to eq('post new')
      expect(@post.body).to eq('this is a body part')
      expect(response).to redirect_to(topic_post_url(@topic, @post))
      expect(@post.tags.count).to be(1)
    end

    it 'ensures post is created with image upload' do
      file = fixture_file_upload('post_image_1.jpeg', 'image/jpg')
      puts fixture_path
      params = {
        post: {
          title: 'topic 1',
          body: 'this is a new post',
          image: file,
          tags_attributes: {
            name: ''
          }
        }
      }
      post "/topics/#{@topic.id}/posts", params: params
      post = Post.where(title: 'topic 1').first
      expect(Post.where(id: post.id).exists?).to eq(true)
      expect(response).to redirect_to(topic_post_path(@topic, post))
      expect(response).to have_http_status(:found)
      expect(post.image.attached?).to be(true)
    end

    it 'ensures post is created with tags assigned' do
      params = {
        post: {
          title: 'topic 1',
          body: 'this is a new post',
          tag_ids: [@tag1.id, @tag2.id],
          tags_attributes: {
            name: 'hello1,science'
          }
        }
      }
      post "/topics/#{@topic.id}/posts", params: params
      post = Post.where(title: 'topic 1').first
      expect(Post.where(id: post.id).exists?).to eq(true)
      expect(response).to redirect_to(topic_post_path(@topic, post))
      expect(response).to have_http_status(:found)
      expect(post.tags.count).to be(4)
    end
  end

  context 'All posts page' do

    before(:each) do
      @topic1 = create(:topic)
      @topic2 = create(:topic)
      @user = create(:user)
      @post1 = create(:post, topic_id: @topic1.id, user_id: @user.id)
      @post2 = create(:post, topic_id: @topic1.id, user_id: @user.id)
      @post3 = create(:post, topic_id: @topic2.id, user_id: @user.id)
      @post4 = create(:post, topic_id: @topic2.id, user_id: @user.id)
      sign_in @user
    end

    it 'ensures all posts route will display all posts' do
      get '/posts'
      expect(response).to render_template(:index)
      expect(response.body).to include('Posts')
      expect(assigns(:posts).count).to eq(Post.all.count)
    end
  end

  context 'Pagination' do
    before(:each) do
      @user = create(:user)
      14.times do
        @topic = create(:topic)
        create(:post, topic_id: @topic.id, user_id: @user.id)
      end
      @posts = Post.all
      sign_in @user
    end

    it 'ensures page 1 has 10 records' do
      get '/posts?page=1'
      expect(response).to render_template(:index)
      expect(assigns(:posts).count).to be(10)
    end

    it 'ensures page 2 has 4 records' do
      get '/posts?page=2'
      expect(response).to render_template(:index)
      expect(assigns(:posts).count).to be(4)
    end

    it 'ensures page 3 has 0 records and empty page returned' do
      get '/posts?page=3'
      expect(response).to render_template(:index)
      expect(assigns(:posts).count).to be(0)
      expect(response.body).to include('No Posts Found....')
    end
  end

  context 'testing user permissions' do

    before(:each) do
      @topic = create(:topic)
      @user = create(:user)
      @user2 = create(:user, email: 'ravi@gmail.com', password: 'userravi')
    end

    it 'can manage user posts' do
      ability = Ability.new(@user2)
      @post = create(:post, topic_id: @topic.id, user_id: @user2.id)
      expect(ability).to be_able_to(:manage, @post)
    end

    it 'cannot go to edit post page, raises error' do
      sign_in @user2
      @post = create(:post, topic_id: @topic.id, user_id: @user.id)
      expect { get edit_topic_post_url(@topic, @post) }.to raise_error(CanCan::AccessDenied)
    end

    it 'cannot edit post, raises error' do
      sign_in @user2
      @post = create(:post, topic_id: @topic.id, user_id: @user.id)
      expect { put topic_post_url(@topic, @post) }.to raise_error(CanCan::AccessDenied)
    end

    it 'cannot be able to edit post' do
      ability = Ability.new(@user2)
      @post = create(:post, topic_id: @topic.id, user_id: @user.id)
      expect(ability).not_to be_able_to(:edit, @post)
    end

    it 'cannot delete post, raises error' do
      sign_in @user2
      @post = create(:post, topic_id: @topic.id, user_id: @user.id)
      expect { delete topic_post_url(@topic, @post) }.to raise_error(CanCan::AccessDenied)
    end

    it 'cannot be able to delete post' do
      ability = Ability.new(@user2)
      @post = create(:post, topic_id: @topic.id, user_id: @user.id)
      expect(ability).not_to be_able_to(:destroy, @post)
    end
  end

  context 'post created through ajax call' do

    before(:each) do
      @topic = create(:topic)
      @user = create(:user)
      sign_in @user
    end

    it 'is new post status is ok' do
      get new_topic_post_url(@topic), xhr: true
      expect(response).to have_http_status(:ok)
    end

    it 'is new post form rendered as js' do
      get new_topic_post_url(@topic), xhr: true
      expect(response).to render_template(:new)
      expect(response.content_type).to eq('text/javascript; charset=utf-8')
    end

    it 'is create js response sent with 200 status code' do
      params = {
        post: {
          title: 'topic 1',
          body: 'this is a new post',
          tags_attributes: {
            name: ''
          }
        }
      }
      post topic_posts_url(@topic), xhr: true, params: params
      expect(response).to have_http_status(:ok)
    end

    it 'is post created successfully' do
      params = {
        post: {
          title: 'topic 1',
          body: 'this is a new post',
          tags_attributes: {
            name: ''
          }
        }
      }
      post topic_posts_url(@topic), xhr: true, params: params
      expect(assigns(:post)).to be_valid
      expect(response.body).to include('Post created successfully')
    end

    it 'is post not created successfully' do
      params = {
        post: {
          title: 'to',
          body: 'this is a new post',
          tags_attributes: {
            name: ''
          }
        }
      }
      post topic_posts_url(@topic), xhr: true, params: params
      expect(assigns(:post)).not_to be_valid
      expect(assigns(:post).errors.full_messages.first).to eq('Title is too short (minimum is 3 characters)')
    end

    it 'is post not created successfully' do
      params = {
        post: {
          title: 'toiwnnwwndnwqdnqwidqwdiqnwo',
          body: 'this is a new post',
          tags_attributes: {
            name: ''
          }
        }
      }
      post topic_posts_url(@topic), xhr: true, params: params
      expect(assigns(:post)).not_to be_valid
      expect(assigns(:post).errors.full_messages.first).to eq('Title is too long (maximum is 20 characters)')
    end

    it 'ensures post not created error status' do
      params = {
        post: {
          title: 'toiwnnwwndnwqdnqwidqwdiqnwo',
          body: 'this is a new post',
          tags_attributes: {
            name: ''
          }
        }
      }
      post topic_posts_url(@topic), xhr: true, params: params
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  context 'renders posts between specified range of date' do

    before(:each) do
      @topic = create(:topic)
      @user = create(:user)
      @post1 = create(:post, topic_id: @topic.id, user_id: @user.id)
      @post2 = create(:post, topic_id: @topic.id, user_id: @user.id)
      @post3 = create(:post, topic_id: @topic.id, user_id: @user.id)
      sign_in @user
    end

    it 'renders 3 posts from specified range of date' do
      get '/posts', params: params = {
        fromDate: '2022-04-14',
        toDate: '2022-04-29'
      }
      expect(assigns(:posts).count).to eq(3)
    end

    it 'renders 0 posts from specified range of date' do
      get '/posts', params: params = {
        fromDate: '2022-04-14',
        toDate: '2022-04-16'
      }
      expect(assigns(:posts).count).to eq(0)
      expect(response.body).to include('No Posts Found....')
    end

    it 'renders 0 posts from specified range of date in opposite' do
      get '/posts', params: params = {
        fromDate: '2022-04-16',
        toDate: '2022-04-10'
      }
      expect(assigns(:posts).count).to eq(0)
      expect(response.body).to include('No Posts Found....')
    end

    it 'renders 3 posts if no dates specified' do
      get '/posts'
      expect(assigns(:posts).count).to eq(3)
    end
  end

  context 'Counter Cache' do

    before(:each) do
      @topic = create(:topic)
      @user = create(:user)
      @post = create(:post, topic_id: @topic.id, user_id: @user.id)
      sign_in @user
    end

    it 'ensures comments count is zero initially' do
      get topic_post_url(@topic, @post)
      expect(assigns(:post).comments_count).to eq(0)
    end

    it 'ensures comments count is updated' do
      comment1 = create(:comment, post_id: @post.id, user_id: @user.id)
      comment2 = create(:comment, post_id: @post.id, user_id: @user.id)
      comment3 = create(:comment, post_id: @post.id, user_id: @user.id)
      get topic_post_url(@topic, @post)
      expect(assigns(:post).comments_count).to eq(3)
    end
  end

  context 'Ratings Average Caching' do

    before(:each) do
      @topic = create(:topic)
      @user = create(:user)
      @post = create(:post, topic_id: @topic.id, user_id: @user.id)
      sign_in @user
    end

    it 'ensures comments count is zero initially' do
      get topic_post_url(@topic, @post)
      expect(assigns(:post).rating_average).to eq(0.0)
    end

    it 'ensures ratings average is updated' do
      rating1 = create(:rating, point: 1, post_id: @post.id)
      rating2 = create(:rating, point: 5, post_id: @post.id)
      rating3 = create(:rating, point: 3, post_id: @post.id)
      get topic_post_url(@topic, @post)
      @post.reload
      expect(assigns(:post).rating_average).to eq(@post.ratings.average(:point).to_f.floor(1))
    end
  end
end
