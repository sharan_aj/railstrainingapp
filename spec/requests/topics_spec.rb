require 'rails_helper'

RSpec.describe 'Topic', type: :request do

  context 'unauthorized user' do

    before(:each) do
      @topic = create(:topic)
    end

    it 'redirects to sign in page' do
      get topics_url
      expect(response).to have_http_status(302)
      expect(response).to redirect_to(new_user_session_url)
    end

    it 'redirects to sign in page' do
      get new_topic_url
      expect(response).to have_http_status(302)
      expect(response).to redirect_to(new_user_session_url)
    end

    it 'redirects to sign in page' do
      get topic_url(@topic)
      expect(response).to have_http_status(302)
      expect(response).to redirect_to(new_user_session_url)
    end

    it 'redirects to sign in page' do
      get edit_topic_url(@topic)
      expect(response).to have_http_status(302)
      expect(response).to redirect_to(new_user_session_url)
    end
  end

  context 'GET index route' do

    before(:each) do
      sign_in create(:user)
    end

    it 'has 200 status code' do
      get '/'
      expect(response).to have_http_status(:ok)
    end

    it 'has to index.html page' do
      get '/topics'
      expect(response).to render_template(:index)
      expect(response.content_type).to eq 'text/html; charset=utf-8'
    end
  end

  context 'GET new' do

    before(:each) do
      sign_in create(:user)
    end

    it 'has 200 status code' do
      get '/topics/new'
      expect(response).to have_http_status(:ok)
    end

    it 'has to render new.html' do
      get '/topics/new'
      expect(response).to render_template(:new)
    end
  end

  context 'GET show' do

    before(:each) do
      @topic1 = create(:topic)
      sign_in create(:user)
    end

    it 'has status code of 200' do
      get "/topics/#{@topic1.id}"
      expect(response).to have_http_status(:found)
    end

    it 'has to render show.html' do
      get "/topics/#{@topic1.id}"
      expect(response).to redirect_to(topic_posts_url(@topic1))
    end

    it 'has to render show.html with error message of no topic found' do
      get '/topics/100'
      expect(response).to render_template(:show)
      expect(response.body).to include("Topic doesn't exist")
    end
  end

  context 'GET edit' do

    before(:each) do
      @topic1 = create(:topic)
      sign_in create(:user)
    end

    it 'has to be rendered edit.html with status of ok' do
      get "/topics/#{@topic1.id}/edit"
      expect(response).to render_template(:edit)
      expect(response).to have_http_status(:ok)
    end

    it 'has to be rendered edit.html with status of ok' do
      get '/topics/100/edit'
      expect(response).to render_template(:edit)
      expect(response.body).to include("Topic doesn't exist")
    end
  end

  context 'PUT/PATCH update' do

    before(:each) do
      @topic1 = create(:topic)
      sign_in create(:user)
    end

    it 'on form submitted topic should update and redirected to show' do
      params = {
        topic: {
          title: 'title-null'
        }
      }
      put "/topics/#{@topic1.id}", params: params
      @topic1.reload
      expect(@topic1.title).to eq('title-null')
      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(topic_url(@topic1))
    end

    it "on form submitted topic shouldn't update and redirected to index with message" do
      params = {
        topic: {
          title: 'title-null'
        }
      }
      put '/topics/100', params: params
      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(topics_url)
      expect(flash[:notice]).to eq('Topic is not found')
    end

    it "on form submitted topic shouldn't update and render edit page with error" do
      params = {
        topic: {
          title: ''
        }
      }
      put "/topics/#{@topic1.id}", params: params
      expect(response).to have_http_status(:unprocessable_entity)
      expect(response).to render_template(:edit)
      expect(assigns(:topic)).not_to be_valid
      expect(assigns(:topic).errors.full_messages).to include("Title can't be blank")
    end

    it "on form submitted topic shouldn't update and render edit page with error" do
      params = {
        topic: {
          title: 'hi'
        }
      }
      put "/topics/#{@topic1.id}", params: params
      expect(response).to have_http_status(:unprocessable_entity)
      expect(response).to render_template(:edit)
      expect(assigns(:topic)).not_to be_valid
      expect(assigns(:topic).errors.full_messages).to include('Title is too short (minimum is 3 characters)')
    end
  end

  context 'Create a new resource using POST method' do

    before(:each) do
      sign_in create(:user)
    end

    it 'on form submitted new topic to be created and redirected to show' do
      params = {
        topic: {
          title: 'title1'
        }
      }
      post '/topics', params: params
      topic = Topic.where(title: 'title1').first
      expect(topic).to be_valid
      expect(topic.title).to eq(params[:topic][:title])
      expect(response).to redirect_to(topic_url(topic))
      expect(response).to have_http_status(:found)
      expect(Topic.all.count).to be(1)
    end

    it 'on form submitted new topic to be created and redirected to show' do
      params = {
        topic: {
          title: ''
        }
      }
      post '/topics', params: params
      expect(response).to render_template(:new)
      expect(response).to have_http_status(:unprocessable_entity)
      expect(Topic.all.count).to be(0)
      expect(assigns(:topic).errors.full_messages).to include("Title can't be blank")
      expect(assigns(:topic).errors.full_messages).to include('Title is too short (minimum is 3 characters)')
    end

    it 'on form submitted new topic to be created and redirected to show' do
      params = {
        topic: {
          title: 'hi'
        }
      }
      post '/topics', params: params
      expect(response).to render_template(:new)
      expect(response).to have_http_status(:unprocessable_entity)
      expect(Topic.all.count).to be(0)
      expect(assigns(:topic).errors.full_messages).to include('Title is too short (minimum is 3 characters)')
    end
  end

  context 'Destroy a resource using DELETE method' do

    before(:each) do
      @topic1 = create(:topic)
      sign_in create(:user)
    end

    it 'On click Topic should be deleted' do
      delete "/topics/#{@topic1.id}"

      expect(response).to redirect_to(topics_url)
      expect(response).to have_http_status(:found)
      expect(Topic.find_by(id: @topic1.id)).to be(nil)
    end

    it 'On clicking delete button returns error message in topics url' do
      delete '/topics/100'

      expect(response).to redirect_to(topics_url)
      expect(response).to have_http_status(:found)
      expect(flash[:notice]).to eq('Topic is not found')
      expect(Topic.count).to be(1)
    end
  end

  context 'Testing JSON responses' do

    before(:each) do
      @topic = create(:topic)
      @user = create(:user)
      sign_in @user
    end

    it 'ensures response type in topics index page' do
      get '/topics.json'
      expect(response.content_type).to eq('application/json; charset=utf-8')
    end

    it 'ensures response type in topics show page' do
      get topic_url(@topic), headers: {
        accept: 'application/json'
      }
      expect(response.content_type).to eq('application/json; charset=utf-8')
    end

    it 'ensures response type in topics show page' do
      post topics_url, headers: {
        accept: 'application/json'
      }, params: {
        topic: {
          title: 'new title'
        }
      }
      expect(response.content_type).to eq('application/json; charset=utf-8')
    end

    it 'ensures response body title is valid' do
      post topics_url, headers: {
        accept: 'application/json'
      }, params: {
        topic: {
          title: 'new title'
        }
      }
      topic = assigns(:topic)
      expect(JSON.parse(response.body)['title']).to eq(topic.title)
    end

    it 'ensures response type in topics show page' do
      put topic_url(@topic), headers: {
        accept: 'application/json'
      }, params: {
        topic: {
          title: 'new title'
        }
      }
      expect(response.content_type).to eq('application/json; charset=utf-8')
    end

    it 'ensures response body title is updated and json response' do
      put topic_url(@topic), headers: {
        accept: 'application/json'
      }, params: {
        topic: {
          title: 'new title1'
        }
      }
      topic = Topic.find(@topic.id)
      expect(JSON.parse(response.body)['title']).to eq('new title1')
      expect(topic.title).to eq('new title1')
    end

    it 'ensures response body title is deleted and 204 status' do
      delete topic_url(@topic), headers: {
        accept: 'application/json'
      }
      expect(response.body).to eq('')
      expect(response).to have_http_status(:no_content)
    end
  end
end
