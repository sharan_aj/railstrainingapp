require 'rails_helper'

RSpec.describe 'Comments', type: :request do

  require "cancan/matchers"

  before(:each) do
    @topic = create(:topic)
    @user = create(:user)
    @user2 = create(:user, email: 'ravi@gmail.com', password: 'userravi')
    @post = create(:post, topic_id: @topic.id, user_id: @user.id)
    sign_in @user
  end

  it 'expect to create comment through POST create action' do
    params = {
      comment: {
        body: 'new comment'
      }
    }
    post "/topics/#{@topic.id}/posts/#{@post.id}/comments", params: params
    expect(response).to redirect_to(topic_post_url(@post.topic, @post))
    @post.reload
    expect(@post.comments_count).to eq(1)
  end

  it 'expect to be comment not save and redirect with a error' do
    params = {
      comment: {
        body: ''
      }
    }
    post "/topics/#{@topic.id}/posts/#{@post.id}/comments", params: params
    expect(response).to redirect_to(topic_post_url(@post.topic, @post))
    expect(flash[:error]).to eq('Your Comment is empty')
  end

  it 'expect to be comment to be destroyed and redirect to post' do
    @comment = create(:comment, post_id: @post.id, user_id: @user.id)
    delete "/topics/#{@topic.id}/posts/#{@post.id}/comments/#{@comment.id}"
    expect(response).to redirect_to(topic_post_url(@post.topic, @post))
    expect(@post.comments).to be_empty
    expect(@post.comments_count).to eq(0)
  end

  it 'can manage user posts' do
    ability = Ability.new(@user2)
    @comment = create(:comment, post_id: @post.id, user_id: @user2.id)
    expect(ability).to be_able_to(:manage, @comment)
  end

  it 'cannot delete comment, raises error' do
    sign_out @user
    sign_in @user2
    @comment = create(:comment, post_id: @post.id, user_id: @user.id)
    expect { delete "/topics/#{@topic.id}/posts/#{@post.id}/comments/#{@comment.id}" }.to raise_error(CanCan::AccessDenied)
  end

  it 'cannot be able to delete comment' do
    ability = Ability.new(@user2)
    @comment = create(:comment, post_id: @post.id, user_id: @user.id)
    expect(ability).not_to be_able_to(:destroy, @comment)
  end
end
