class Rating < ApplicationRecord
  belongs_to :post
  validates :point, presence: true
  validates :point, numericality: { only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 5 }

  after_create_commit :rating_callback

  def rating_callback
    post.update_attribute(:rating_average, post.ratings.average(:point).to_f.floor(1))
  end
end
