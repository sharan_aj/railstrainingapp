class User < ApplicationRecord
  has_many :posts
  has_many :comments
  has_and_belongs_to_many :read_posts, class_name: 'Post'
  has_many :user_comment_ratings, through: :comments
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable

  after_create :send_mail

  def send_mail
    SendEmailJob.perform_later(self)
  end
end
