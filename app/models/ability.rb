# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    can :read, :all
    can :manage, Post, user_id: user.id
    can :user_status, Post
    can :manage, Comment, user_id: user.id
  end
end
