class Comment < ApplicationRecord
  belongs_to :user
  has_many :user_comment_ratings, dependent: :destroy
  belongs_to :post, counter_cache: true
  validates :body, presence: true
end
