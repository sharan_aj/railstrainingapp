class Post < ApplicationRecord
  belongs_to :topic
  belongs_to :user
  has_and_belongs_to_many :users
  has_many :comments, dependent: :destroy
  has_many :ratings, dependent: :destroy
  has_and_belongs_to_many :tags
  has_one_attached :image
  validates_associated :tags
  accepts_nested_attributes_for :tags
  validates :title, presence: true, length: { minimum: 3, maximum: 20 }
  validates :body, presence: true, length: { minimum: 3 }
  validate :acceptable_image

  scope :filter_date, ->(fromDate, toDate) { where('created_at >= ? AND created_at <= ?',fromDate,toDate.end_of_day) }

  def acceptable_image
    return unless image.attached?

    unless image.byte_size <= 1.megabyte
      errors.add(:image, 'size is too big,should be less than 1 MB')
    end

    acceptable_types = ['image/jpeg', 'image/png']
    unless acceptable_types.include?(image.content_type)
      errors.add(:image, 'must be png or jpeg')
    end
  end
end
