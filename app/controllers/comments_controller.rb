class CommentsController < ApplicationController
  before_action :get_topic
  before_action :get_post
  load_and_authorize_resource

  def create
    @comment = @post.comments.new(comment_params.merge(user_id: current_user.id))
    if @comment.save
      redirect_to topic_post_path(@post.topic, @post)
    else
      redirect_to topic_post_path(@post.topic, @post), error: 'Your Comment is empty'
    end
  end

  def destroy
    comment = @post.comments.find(params[:id])
    if comment.destroy
      redirect_to topic_post_path(@post.topic, @post)
    else
      redirect_to topic_post_path(@post.topic, @post), error: "Comment can't be deleted try again"
    end
  end

  private

  def get_topic
    @topic = Topic.find_by(id: params[:topic_id])
  end

  def get_post
    @post = @topic.posts.find_by(id: params[:post_id])
  end

  def comment_params
    params.require(:comment).permit(:body)
  end
end
