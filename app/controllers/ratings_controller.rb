class RatingsController < ApplicationController

  def create
    @post = Post.find(params[:post_id])
    if @post.ratings.create(post_rating)
      redirect_to topic_post_path(@post.topic, @post)
    else
      redirect_to topic_post_path(@post.topic, @post), info: @rating.errors.full_messages.first
    end
  end

  private

  def post_rating
    params.require(:rating).permit(:point)
  end
end
