class UserCommentRatingsController < ApplicationController
  before_action :set_topic
  before_action :set_post
  before_action :set_comment

  def index
    @ratings = @comment.user_comment_ratings.includes(:user)
  end

  def create
    @rating = @comment.user_comment_ratings.new(rating_params.merge(user_id: current_user.id))
    if @rating.save
      redirect_to topic_post_path(@post.topic, @post)
    else
      redirect_to topic_post_path(@post.topic, @post), info: @rating.errors.full_messages.first
    end
  end

  private

  def set_topic
    @topic = Topic.find_by(id: params[:topic_id])
  end

  def set_post
    @post = @topic.posts.find_by(id: params[:post_id])
  end

  def set_comment
    @comment = @post.comments.find_by(id: params[:comment_id])
  end

  def rating_params
    params.require(:user_comment_rating).permit(:point)
  end
end
