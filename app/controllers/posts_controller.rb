class PostsController < ApplicationController
  before_action :get_topic
  skip_before_action :verify_authenticity_token, only: [:user_status]
  before_action :get_post, only: %i[show edit update destroy]
  load_and_authorize_resource

  # GET /topics/:topic_id/posts
  def index
    if params[:fromDate].present? && params[:toDate].present?
      @pagy, @posts = @topic ? pagy(@topic.posts, items: 10) : pagy(Post.includes(:topic).filter_date(params[:fromDate], params[:toDate].to_date), items: 10)
      @read_posts = current_user.read_posts.to_a
    else
      @pagy, @posts = @topic ? pagy(@topic.posts, items: 10) : pagy(Post.includes(:topic).filter_date(Date.yesterday, Date.today), items: 10)
      @read_posts = current_user.read_posts.to_a
    end
  end

  # GET /topics/:topic_id/posts/:id
  def show
      @comments = @post.comments.includes(:user)
      @post_tags = @post.tags
      @ratings = @post.ratings.group(:point).order(:point).count
  end

  # GET /topics/:topic_id/posts/new
  def new
    @post = @topic ? @topic.posts.new : nil
  end

  # GET /topics/:topic_id/post/:id/edit
  def edit; end

  # POST /topics/:topic_id/posts
  def create
    @post = @topic.posts.new(post_params.merge(user_id: current_user.id))
    @post.tags_attributes = get_tags if get_tags
    respond_to do |format|
      if @post.save
        format.html { redirect_to topic_post_path(@topic, @post), notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
        format.js
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
        format.js { render :create, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /topics/:topic_id/posts/:id
  def update
    respond_to do |format|
      if @post
        if @post.update(post_params)
          format.html { redirect_to topic_post_path(@topic, @post), notice: 'Post was successfully updated.' }
          format.json { render :show, status: :ok, location: @post }
        else
          format.html { render :edit, status: :unprocessable_entity }
          format.json { render json: @post.errors, status: :unprocessable_entity }
        end
      else
        format.html { redirect_to topic_posts_path(@topic), notice: 'Post not found' }
      end
    end
  end

  def destroy
    if @post
      @post.destroy
      respond_to do |format|
        format.html { redirect_to topic_posts_path(@topic), notice: 'Post was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to topic_posts_url(@topic), notice: 'Post not found'
    end
  end

  def user_status
    @post = @topic.posts.find(params[:id])
    unless @post.users.where(email: current_user.email).exists?
      @post.users << current_user
    end
  end

  private

  def get_topic
    @topic = Topic.find_by(id: params[:topic_id])
  end

  def get_post
    @post = @topic.posts.find_by(id: params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :body, :image, tag_ids: [])
  end

  def get_tags
    tags = []
    params[:post][:tags_attributes][:name].split(',').each do |p|
      tags.push({ name: p })
    end
    tags
  end
end
