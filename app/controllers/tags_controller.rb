class TagsController < ApplicationController

  def index
    @tags = Tag.all
    @tag = !params[:id] ? Tag.new : Tag.find_by(id: params[:id])
  end

  def create
    @tag = Tag.new(tag_params)
    if @tag.save
      redirect_to tags_url, notice: 'Tag Created Successfully'
    else
      redirect_to tags_url, error: @tag.errors.full_messages.first
    end
  end

  def update
    @tag = Tag.find_by(id: params[:id])
    if @tag.update(tag_params)
      redirect_to tags_url, notice: 'Tag Updated Successfully'
    else
      redirect_to tags_url(id: @tag.id), info: params[:tag][:name], error: @tag.errors.full_messages.first
    end
  end

  def destroy
    tag = Tag.find_by(id: params[:id])
    tag.destroy
    redirect_to tags_url
  end

  private

  def tag_params
    params.require(:tag).permit(:name)
  end
end
