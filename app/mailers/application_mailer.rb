class ApplicationMailer < ActionMailer::Base
  default from: 'sharan.jayakumar@mallow-tech.com'
  layout 'mailer'
end
