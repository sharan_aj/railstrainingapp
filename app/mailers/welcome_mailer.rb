class WelcomeMailer < ApplicationMailer

  def welcome_mail(user)
    @user = user
    @url = root_url
    mail to: @user.email, subject: 'Welcome to my site'
  end
end
