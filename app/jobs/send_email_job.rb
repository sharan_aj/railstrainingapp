class SendEmailJob < ApplicationJob
  queue_as :default

  def perform(user)
    WelcomeMailer.welcome_mail(user).deliver
  end
end
